################################
Python for Power System Dynamics
################################


About
=====

PyPSD stands for "Python for Power System Dynamics".

PyPSD is a `free software
<http://www.gnu.org/philosophy/free-sw.en.html>`_ toolbox for studying the 
dynamics of modern power systems as a whole. It is intended to be used in 
conjunction with `PyPSA <https://pypsa.org/>`_

As of 2017 PyPSD is under heavy development and is not recommended for 
production use. All APIs are liable to change.

PyPSD is being developed by the `Potsdam Institute for Climate Impact Research 
<https://pik-potsdam.de/>`_ to develop holistic dynamic stability concepts for
future power grids. This work is part of the `CoNDyNet project
<http://condynet.de/>`_, financed by the `German Federal Ministry for Education
and Research (BMBF) <https://www.bmbf.de/en/index.html>`_ as part of the 
`Stromnetze Research Initiative <http://forschung-stromnetze.info/projekte/grundlagen-und-konzepte-fuer-effiziente-dezentrale-stromnetze/>`_.

