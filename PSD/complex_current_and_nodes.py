from __future__ import absolute_import, division, print_function, unicode_literals
import numpy as np
from numba import njit, float64, complex128, void, int32, __version__
# from assimulo.solvers import CVODE
# from assimulo.problem import Explicit_Problem
import scipy.sparse as sps
import os

# numba version on the cluster does not support caching
cache_flag = __version__ != '0.19.1'

# The dynamics should be structured according to what is calculated on the network links.

class NodeType(object):
    def __init__(self, number_of_variables):
        self.number_of_variables = number_of_variables


class SwingEquationNode(NodeType):
    def __init__(self, number_of_variables, v_set = 1., infeed = 1., damping = 0.1, H = 1.5):
        super(SwingEquationNode, self).__init__(number_of_variables)
        self.v_set = v_set
        self.v_set_squared = v_set ** 2
        self.infeed = infeed
        self.damping = damping
        self.H_inv = 1./H

    def node_dynamics(self, v, omega, i, t, dv, domega, index):
        # The current is assumed to be negative when it is flowing away from the node
        dv[index] = 1.j * omega[index] * v[index] - (v[index].real * v[index].real + v[index].imag * v[index].imag - self.v_set_squared) * v[index]
        domega[index] = self.H_inv * (self.infeed - self.damping * omega[index] - (v[index] * i[index].conjugate()).real)

    def node_dynamics_string(self, j="{index}"):
        return """
    dv[{index}] = 1.j * omega[{index}] * v[{index}] - (v[{index}].real * v[{index}].real + v[{index}].imag * v[{index}].imag - {v_set_squared}) * v[{index}]
    domega[{index}] = {H_inv:.64f} * ({infeed} - {damping} * omega[{index}] - (v[{index}] * i[{index}].conjugate()).real)""" \
            .format(
            v_set_squared=self.v_set_squared,
            H_inv=self.H_inv,
            infeed=self.infeed,
            damping=self.damping,
            index=j)


class DroopEquationNode(NodeType):
    def __init__(self, number_of_variables, v_set = 1., infeed = 1., damping = 0.1, H = 1.5):
        super(DroopEquationNode, self).__init__(number_of_variables)
        self.v_set = v_set
        self.v_set_squared = v_set ** 2
        self.infeed = infeed
        self.damping = damping
        self.H_inv = 1./H

    def node_dynamics(self, v, omega, i, t, dv, domega, index):
        # TODO Jonathan: Implement the Schiffer equations here:
        # The current is assumed to be negative when it is flowing away from the node
        dv[index] = 1.j * omega[index] * v[index] - (v[index].real * v[index].real + v[index].imag * v[index].imag - self.v_set_squared) * v[index]
        domega[index] = self.H_inv * (self.infeed - self.damping * omega[index] - (v[index] * i[index].conjugate()).real)

    def node_dynamics_string(self, j="{index}"):
        return """
    dv[{index}] = 1.j * omega[{index}] * v[{index}] - (v[{index}].real * v[{index}].real + v[{index}].imag * v[{index}].imag - {v_set_squared}) * v[{index}]
    domega[{index}] = {H_inv:.64f} * ({infeed} - {damping} * omega[{index}] - (v[{index}] * i[{index}].conjugate()).real)""" \
            .format(
            v_set_squared=self.v_set_squared,
            H_inv=self.H_inv,
            infeed=self.infeed,
            damping=self.damping,
            index=j)


@njit(complex128[:](complex128[:], int32[:], int32[:], complex128[:]))
def numba_sp_dot(data, indptr, indices, v):
    res = np.zeros(len(indptr) - 1, dtype=np.complex128)
    index = 0
    for row, number_of_entries in enumerate(indptr[1:]):
        while index < number_of_entries:
            res[row] += data[index] * v[indices[index]]
            index += 1
    return res


@njit
def numba_sp_complex_currents(data, indptr, indices, v, i):
    index = 0
    for row, number_of_entries in enumerate(indptr[1:]):
        r_temp = 0.
        while index < number_of_entries:
            r_temp += data[index] * v[indices[index]]
            index += 1
        i[row] = r_temp


def define_network_rhs(node_list, Y):
    assert len(node_list) == Y.shape[0]
    length = Y.shape[0]
    total_length = 2 * length

    def network_rhs(y, t):
        coupling_sp = sps.csr_matrix(Y)
        dydt = np.empty(total_length + length)

        v = y[:total_length].view(np.complex128)
        omega = y[total_length:]
        dv = dydt[:total_length].view(np.complex128)
        domega = dydt[total_length:]

        i = coupling_sp.dot(v)

        for j, node in enumerate(node_list):
            node.node_dynamics(v, omega, i, t, dv, domega, j)
        return dydt

    return network_rhs


def define_root_rhs(total_length, func):
    rhs = lambda x: func(x, 0)
    def root_rhs(y, rhs=rhs):
        dydt = rhs(y)
        v = y[:total_length].view(np.complex128)
        dv = dydt[:total_length].view(np.complex128)
        domega = dydt[total_length:]
        if v[0].imag >= v[0].real:
            omega = -1. * dv[0].real/v[0].imag
        else:
            omega = dv[0].imag/v[0].real
        a = 1.j * omega * v - dv
        return np.concatenate((domega, a.view(np.float64)))
    return root_rhs

# Another try at a reasonable root finding function. This one just adds omega_global as a variable,
# and adds a zero to the return
def define_root_rhs_omega(total_length, rhs):
    zero = np.zeros(1, dtype=np.float64)
    def root_rhs(y):
        dydt = rhs(y[:-1], 0.)
        v = y[:total_length].view(np.complex128)
        dv = dydt[:total_length].view(np.complex128)
        domega = dydt[total_length:]
        omega_global = y[-1]
        a = 1.j * omega_global * v - dv
        return np.concatenate((zero, domega, a.view(np.float64)))
    return root_rhs


def define_network_rhs_codegen(node_list, Y):
    network_rhs_numba = None

    assert len(node_list) == Y.shape[0]
    length = Y.shape[0]
    total_length = 2*length

    coupling_sp = sps.csr_matrix(Y)

    data = coupling_sp.data
    indptr = coupling_sp.indptr
    l_indptr = len(indptr)
    indices = coupling_sp.indices

    file_header = """
import numpy as np
from numba import njit, float64, complex128, void, int32

    """

    if cache_flag:
        def_network_rhs_string = "@njit(float64[:](float64[:], float64), cache=True)"
    else:
        def_network_rhs_string = "@njit(float64[:](float64[:], float64))"

    def_network_rhs_string += """
def network_rhs_numba(y, t):
    dydt = np.empty({total_length} + {length})

    v = y[:{total_length}].view(np.complex128)
    omega = y[{total_length}:]
    dv = dydt[:{total_length}].view(np.complex128)
    domega = dydt[{total_length}:]

    i = np.zeros({l_indptr} - 1, dtype=np.complex128)
    index = 0
    for row, number_of_entries in enumerate(indptr[1:]):
        while index < number_of_entries:
            i[row] += data[index] * v[indices[index]]
            index += 1

    """.format(total_length=total_length,
               length=length,
               l_indptr=l_indptr
               )

    def_network_rhs_string += "".join([node.node_dynamics_string(j) for j, node in enumerate(node_list)])

    def_network_rhs_string += """
    return dydt
    """

    cdir = os.path.join(os.getcwd(), "__psdcache__")
    if not os.path.exists(cdir):
        os.mkdir(cdir)
        os.mknod(os.path.join(cdir, "__init__.py"))

    with open(os.path.join(cdir, "compile_function.py"), "w") as text_file:
        text_file.write(file_header)
        text_file.write("\n")
        text_file.write("data=np.array({})".format(data.tolist()))
        text_file.write("\n")
        text_file.write("indptr=np.array({})".format(indptr.tolist()))
        text_file.write("\n")
        text_file.write("indices=np.array({})".format(indices.tolist()))
        text_file.write("\n\n")
        text_file.write(def_network_rhs_string)

    from __psdcache__.compile_function import network_rhs_numba

    return network_rhs_numba

if __name__ == "__main__":

    node_list = list()
    node_list.append(SwingEquationNode(3, infeed=1.))
    node_list.append(SwingEquationNode(3, infeed=-1))

    Y = -8.j * np.ones((2, 2), dtype=np.complex128)
    Y[0, 0] *= -1.
    Y[1, 1] *= -1.

    #rhs = define_network_rhs(node_list, Y)
    rhs = define_network_rhs_codegen(node_list, Y)
    #
    # for i in range(10):
    #     ic = np.random.rand(6)
    #     print(rhs(ic, 0.) - nrhs(ic, 0.))

    root_rhs_1 = lambda x : rhs(x, 0.)

    root_rhs = define_root_rhs_omega(2*len(Y), rhs)

    guess = np.ones(7, dtype=np.float64) * 0.5

    print(root_rhs(guess))

    from scipy.optimize import root

    result = root(root_rhs, guess)
    if result.success:
        print(rhs(result.x[:-1], 0.))
        ic = result.x[:-1]
        print(root_rhs(result.x))
        # ic[5] += 1.
    else:
        print("failed")
        ic = guess[:-1]


    times = np.linspace(0., 100., 1000)

    from scipy.integrate import odeint

    states = odeint(rhs, ic, times)

    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    plt.figure()
    plt.plot(times, states[:, 4:])
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot(states[:, 0], states[:, 1], times)
    ax.plot(states[:, 2], states[:, 3], times)
    plt.show()