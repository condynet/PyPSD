graph [
  node [
    id 0
    label "740"
    width "0.75"
    pos "55748,1.8076e+05"
    height "0.5"
  ]
  node [
    id 1
    label "741"
    width "0.75"
    pos "66706,1.7882e+05"
    height "0.5"
  ]
  node [
    id 2
    label "742"
    width "0.75"
    pos "32311,62360"
    height "0.5"
  ]
  node [
    id 3
    label "744"
    width "0.75"
    pos "14504,96473"
    height "0.5"
  ]
  node [
    id 4
    label "709"
    width "0.75"
    pos "28165,1.2105e+05"
    height "0.5"
  ]
  node [
    id 5
    label "704"
    width "0.75"
    pos "66896,61234"
    height "0.5"
  ]
  node [
    id 6
    label "705"
    width "0.75"
    pos "39701,63462"
    height "0.5"
  ]
  node [
    id 7
    label "706"
    width "0.75"
    pos "92586,43029"
    height "0.5"
  ]
  node [
    id 8
    label "707"
    width "0.75"
    pos "1.1025e+05,65553"
    height "0.5"
  ]
  node [
    id 9
    label "701"
    width "0.75"
    pos "29511,43674"
    height "0.5"
  ]
  node [
    id 10
    label "702"
    width "0.75"
    pos "46795,69393"
    height "0.5"
  ]
  node [
    id 11
    label "703"
    width "0.75"
    pos "26265,1.0081e+05"
    height "0.5"
  ]
  node [
    id 12
    label "708"
    width "0.75"
    pos "32176,1.2794e+05"
    height "0.5"
  ]
  node [
    id 13
    label "799"
    width "0.75"
    pos "18455,18"
    height "0.5"
  ]
  node [
    id 14
    label "731"
    width "0.75"
    pos "12602,1.2504e+05"
    height "0.5"
  ]
  node [
    id 15
    label "730"
    width "0.75"
    pos "28497,1.1637e+05"
    height "0.5"
  ]
  node [
    id 16
    label "733"
    width "0.75"
    pos "31750,1.3584e+05"
    height "0.5"
  ]
  node [
    id 17
    label "732"
    width "0.75"
    pos "39799,1.2744e+05"
    height "0.5"
  ]
  node [
    id 18
    label "735"
    width "0.75"
    pos "21452,1.6246e+05"
    height "0.5"
  ]
  node [
    id 19
    label "734"
    width "0.75"
    pos "33138,1.5017e+05"
    height "0.5"
  ]
  node [
    id 20
    label "737"
    width "0.75"
    pos "46437,1.607e+05"
    height "0.5"
  ]
  node [
    id 21
    label "736"
    width "0.75"
    pos "27,1.8214e+05"
    height "0.5"
  ]
  node [
    id 22
    label "738"
    width "0.75"
    pos "52121,1.6832e+05"
    height "0.5"
  ]
  node [
    id 23
    label "718"
    width "0.75"
    pos "67253,48686"
    height "0.5"
  ]
  node [
    id 24
    label "714"
    width "0.75"
    pos "68216,62349"
    height "0.5"
  ]
  node [
    id 25
    label "713"
    width "0.75"
    pos "55560,67874"
    height "0.5"
  ]
  node [
    id 26
    label "712"
    width "0.75"
    pos "39992,57973"
    height "0.5"
  ]
  node [
    id 27
    label "711"
    width "0.75"
    pos "57293,1.7642e+05"
    height "0.5"
  ]
  node [
    id 28
    label "710"
    width "0.75"
    pos "22091,1.5806e+05"
    height "0.5"
  ]
  node [
    id 29
    label "727"
    width "0.75"
    pos "21107,98421"
    height "0.5"
  ]
  node [
    id 30
    label "724"
    width "0.75"
    pos "1.2804e+05,61653"
    height "0.5"
  ]
  node [
    id 31
    label "725"
    width "0.75"
    pos "97094,38854"
    height "0.5"
  ]
  node [
    id 32
    label "722"
    width "0.75"
    pos "1.1069e+05,68190"
    height "0.5"
  ]
  node [
    id 33
    label "720"
    width "0.75"
    pos "87758,57463"
    height "0.5"
  ]
  node [
    id 34
    label "728"
    width "0.75"
    pos "10419,98712"
    height "0.5"
  ]
  node [
    id 35
    label "729"
    width "0.75"
    pos "10020,91389"
    height "0.5"
  ]
  edge [
    source 0
    target 27
    impedance "(2.0952+0.7758j)"
    pos "57286,1.7643e+05 57184,1.7672e+05 55856,1.8046e+05 55754,1.8075e+05"
    len "60.96"
  ]
  edge [
    source 1
    target 27
    impedance "(1.2936+0.6713j)"
    pos "57318,1.7642e+05 57837,1.7656e+05 66161,1.7868e+05 66681,1.7882e+05"
    len "121.92"
  ]
  edge [
    source 2
    target 6
    impedance "(2.0952+0.7758j)"
    pos "39675,63458 39212,63389 32803,62434 32337,62364"
    len "97.536"
  ]
  edge [
    source 3
    target 29
    impedance "(1.2936+0.6713j)"
    pos "21082,98414 20658,98288 14951,96605 14529,96481"
    len "85.344"
  ]
  edge [
    source 3
    target 34
    impedance "(2.0952+0.7758j)"
    pos "14483,96485 14182,96650 10742,98535 10441,98700"
    len "60.96"
  ]
  edge [
    source 3
    target 35
    impedance "(2.0952+0.7758j)"
    pos "14490,96457 14224,96157 10298,91704 10034,91404"
    len "85.344"
  ]
  edge [
    source 4
    target 14
    impedance "(1.2936+0.6713j)"
    pos "28139,1.2106e+05 27461,1.2123e+05 13308,1.2486e+05 12628,1.2504e+05"
    len "182.88"
  ]
  edge [
    source 4
    target 15
    impedance "(1.2936+0.6713j)"
    pos "28495,1.1638e+05 28474,1.1669e+05 28187,1.2073e+05 28166,1.2104e+05"
    len "60.96"
  ]
  edge [
    source 4
    target 12
    impedance "(1.2936+0.6713j)"
    pos "28175,1.2107e+05 28386,1.2143e+05 31953,1.2756e+05 32166,1.2792e+05"
    len "97.536"
  ]
  edge [
    source 5
    target 24
    impedance "(2.0952+0.7758j)"
    pos "66914,61248 67057,61370 68057,62215 68199,62335"
    len "24.384"
  ]
  edge [
    source 5
    target 25
    impedance "(1.2936+0.6713j)"
    pos "55580,67862 56099,67558 66355,61550 66876,61246"
    len "158.496"
  ]
  edge [
    source 5
    target 33
    impedance "(1.2936+0.6713j)"
    pos "66923,61229 67731,61083 86926,57614 87732,57468"
    len "243.84"
  ]
  edge [
    source 6
    target 26
    impedance "(2.0952+0.7758j)"
    pos "39702,63444 39720,63113 39974,58323 39991,57991"
    len "73.152"
  ]
  edge [
    source 6
    target 10
    impedance "(2.0952+0.7758j)"
    pos "46778,69378 46407,69068 40088,63786 39718,63476"
    len "121.92"
  ]
  edge [
    source 7
    target 31
    impedance "(2.0952+0.7758j)"
    pos "92602,43014 92883,42754 96795,39131 97078,38869"
    len "85.344"
  ]
  edge [
    source 7
    target 33
    impedance "(1.2936+0.6713j)"
    pos "87764,57445 87950,56889 92396,43598 92580,43046"
    len "182.88"
  ]
  edge [
    source 8
    target 30
    impedance "(2.0952+0.7758j)"
    pos "1.1027e+05,65547 1.1101e+05,65386 1.2728e+05,61820 1.2802e+05,61659"
    len "231.648"
  ]
  edge [
    source 8
    target 32
    impedance "(2.0952+0.7758j)"
    pos "1.1025e+05,65571 1.1029e+05,65792 1.1065e+05,67951 1.1069e+05,68171"
    len "36.576"
  ]
  edge [
    source 8
    target 33
    impedance "(2.0952+0.7758j)"
    pos "87782,57472 88585,57761 1.0942e+05,65255 1.1022e+05,65544"
    len "280.416"
  ]
  edge [
    source 9
    target 10
    impedance "(0.4751+0.2973j)"
    pos "29523,43691 30011,44418 46299,68654 46784,69376"
    len "292.608"
  ]
  edge [
    source 9
    target 13
    impedance "(0.29+0.195j)"
    pos "18459,35.845 18709,1023.4 29257,42671 29507,43657"
    len "563.88"
  ]
  edge [
    source 10
    target 25
    impedance "(1.2936+0.6713j)"
    pos "46822,69388 47330,69300 55027,67966 55534,67878"
    len "109.728"
  ]
  edge [
    source 10
    target 11
    impedance "(0.4751+0.2973j)"
    pos "46784,69409 46260,70212 26803,99991 26276,1.008e+05"
    len "402.336"
  ]
  edge [
    source 11
    target 29
    impedance "(2.0952+0.7758j)"
    pos "26243,1.008e+05 25890,1.0064e+05 21482,98595 21129,98431"
    len "73.152"
  ]
  edge [
    source 11
    target 15
    impedance "(1.2936+0.6713j)"
    pos "26268,1.0083e+05 26351,1.0141e+05 28411,1.1577e+05 28494,1.1635e+05"
    len "182.88"
  ]
  edge [
    source 12
    target 16
    impedance "(1.2936+0.6713j)"
    pos "32175,1.2796e+05 32153,1.2836e+05 31773,1.3542e+05 31751,1.3583e+05"
    len "97.536"
  ]
  edge [
    source 12
    target 17
    impedance "(2.0952+0.7758j)"
    pos "32203,1.2794e+05 32681,1.279e+05 39294,1.2747e+05 39772,1.2744e+05"
    len "97.536"
  ]
  edge [
    source 16
    target 19
    impedance "(1.2936+0.6713j)"
    pos "31752,1.3586e+05 31806,1.3642e+05 33082,1.4959e+05 33136,1.5015e+05"
    len "170.688"
  ]
  edge [
    source 18
    target 28
    impedance "(2.0952+0.7758j)"
    pos "22088,1.5807e+05 22046,1.5837e+05 21498,1.6215e+05 21455,1.6244e+05"
    len "60.96"
  ]
  edge [
    source 19
    target 20
    impedance "(1.2936+0.6713j)"
    pos "33156,1.5019e+05 33685,1.506e+05 45895,1.6027e+05 46420,1.6069e+05"
    len "195.072"
  ]
  edge [
    source 19
    target 28
    impedance "(2.0952+0.7758j)"
    pos "33120,1.5018e+05 32628,1.5054e+05 22599,1.5769e+05 22109,1.5804e+05"
    len "158.496"
  ]
  edge [
    source 20
    target 22
    impedance "(1.2936+0.6713j)"
    pos "46449,1.6072e+05 46730,1.611e+05 51827,1.6792e+05 52108,1.683e+05"
    len "121.92"
  ]
  edge [
    source 21
    target 28
    impedance "(2.0952+0.7758j)"
    pos "22077,1.5807e+05 21453,1.5875e+05 660.3,1.8145e+05 41.111,1.8213e+05"
    len "390.144"
  ]
  edge [
    source 22
    target 27
    impedance "(1.2936+0.6713j)"
    pos "52131,1.6833e+05 52383,1.6873e+05 57030,1.7601e+05 57282,1.764e+05"
    len "121.92"
  ]
  edge [
    source 23
    target 24
    impedance "(2.0952+0.7758j)"
    pos "68214,62331 68176,61787 67292,49248 67254,48704"
    len "158.496"
  ]
]
